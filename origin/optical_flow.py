#!/usr/bin/python
# -*- coding: utf-8 -*-

# Python 2/3 compatibility
from __future__ import print_function


import numpy as np
import cv2
import time




help_message = \
    '''
USAGE: optical_flow.py [<video_source>]
Keys:
 1 - toggle HSV flow visualization
 2 - toggle glitch
'''
count = 0


def draw_flow(img, flow, step=16):
    
    #from the beginning to position 2 (excluded channel info at position 3)
    h, w = img.shape[:2]
    y, x = np.mgrid[step/2:h:step, step/2:w:step].reshape(2,-1).astype(int)
    fx, fy = flow[y,x].T
    lines = np.vstack([x, y, x+fx, y+fy]).T.reshape(-1, 2, 2)
    lines = np.int32(lines + 0.5)
    vis = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    cv2.polylines(vis, lines, 0, (0, 255, 0))
    for (x1, y1), (x2, y2) in lines:
        cv2.circle(vis, (x1, y1), 1, (0, 255, 0), -1)
    return vis


def draw_hsv(flow):
    (h, w) = flow.shape[:2]
    (fx, fy) = (flow[:, :, 0], flow[:, :, 1])
    ang = np.arctan2(fy, fx) + np.pi
    v = np.sqrt(fx * fx + fy * fy)
    hsv = np.zeros((h, w, 3), np.uint8)
    hsv[..., 0] = ang * (180 / np.pi / 2)
    hsv[..., 1] = 0xFF
    hsv[..., 2] = np.minimum(v * 4, 0xFF)
    bgr = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
    return bgr


def warp_flow(img, flow):
    (h, w) = flow.shape[:2]
    flow = -flow
    flow[:, :, 0] += np.arange(w)
    flow[:, :, 1] += np.arange(h)[:, np.newaxis]
    res = cv2.remap(img, flow, None, cv2.INTER_LINEAR)
    return res

def detect_motion(cap):
    (ret, prev) = cap.read()

    prevgray = cv2.cvtColor(prev, cv2.COLOR_BGR2GRAY)
    motion = 0
    frame_count = 0
    is_detect = False

    while frame_count < 64:
        (ret, img) = cap.read()

        frame_count += 1

        vis = img.copy()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        flow = cv2.calcOpticalFlowFarneback(prevgray, gray, None, 0.5, 5, 15, 3, 5, 1.1, cv2.OPTFLOW_FARNEBACK_GAUSSIAN)
        prevgray = gray
        flow = draw_hsv(flow)

        cv2.imshow('rgb', img)
        cv2.imshow('flow', flow)
        cv2.moveWindow('flow', 660, 20)
        cv2.waitKey(1)

        gray1 = cv2.cvtColor(flow, cv2.COLOR_BGR2GRAY)
        thresh = cv2.threshold(gray1, 25, 0xFF,
                               cv2.THRESH_BINARY)[1]
        thresh = cv2.dilate(thresh, None, iterations=2)
        gray2, cnts, hierarchy = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        if cnts:
            bbox_info = []
            for c in cnts:
                # if the contour is too small, ignore it
                (x, y, w, h) = cv2.boundingRect(c)
                bbox_info.append((x,y,w,h))

            bbox_info_sorted = sorted(bbox_info, key=lambda item: item[2]*item[3], reverse=True)
            w_max, h_max = bbox_info_sorted[0][2], bbox_info_sorted[0][3]

            if w_max > 100 and h_max > 100 and w_max < 640 and h_max < 480:
                motion += 1

        if motion:
            is_detect = True
            cv2.destroyWindow('flow')
            break

    return is_detect