import re
from itertools import permutations

max_seq = 5

def load_data(dir):
    data = []
    with open(dir, 'r') as f:
        while True:
            line = f.readline()
            if not line: break
            data.append(line[:-1])
    return data

def get_permutations(actions):
    result_list = []
    max_len = min([max_seq,len(actions)])   #최대 길이를 몇까지 할 지 결정. 적어도 max_seq보다는 작거나 같아야함.

    for itr in range(max_len):
        p_list = permutations(actions,itr+1)    # [ [action, action, action], [ ... ] ] permutation 결

        for pair in p_list:
            result_list.append(pair)

    return result_list


def make_intention_dictionary(data):
    intent_dict = {}

    #compiler = re.compile('[a-zA-Z+-]')
    str_c = re.compile(' \(.*?\)')      # use lazy quantifier(*?)
    number_c = re.compile('.*?\((.*)\).*') # intention 뒤의 숫자를 가져옴

    candi_actions=[]
    intention =''
    for itr, action in enumerate(data):
        end_test = number_c.search(action)

        if action[-1].isalpha() or end_test.group(1)[-1].isalpha(): #action 중에서도 끝에 괄호를 가진 것이 있어 분리해냄. ex) passing American football (in game)
            candi_actions.append(action)        #액션 리스트에 추가

        else:
            if itr != 0:
                print('intention save ---- : ' + intention + '\n')
                p_actions = get_permutations(candi_actions)
                intent_dict[intention] = p_actions
                candi_actions = []

            intention = re.sub(str_c, "", action)
            print('\ndetect intention : '+str(intention))

    print('intention save ---- : ' + intention + '\n')
    p_actions = get_permutations(candi_actions)
    intent_dict[intention] = p_actions

    return intent_dict

def duplicate(data):
    d_data = []
    if len(data) < max_seq:
        #print('in-------------------------------')
        for itr, action in enumerate(data):
            tmp_data = list(data)
            tmp_data.insert(itr, action)
            #print(data)
            #print(tmp_data)
            d_data.append(tmp_data)

    return d_data

if __name__=='__main__':
    action_list = load_data('./data/action_list.txt')
    pair_dict = make_intention_dictionary(action_list)

    pair_dict_key = pair_dict.keys()

    data_info = []

    print('write action data for each intention')
    #intention 별 action data
    for intention in pair_dict_key:

        with open('./result/intention/'+str(intention), 'w') as f:
            for action in pair_dict[intention]:
                line = ', '.join(action)
                f.write(line+'\n')

                d_line = duplicate(action)

                if d_line:
                    d_line = ', '.join(action)
                    f.write(d_line + '\n')
        data_info.append(len(pair_dict[intention]))

    print('-----------------------------\n')

    print('write data information')

    with open('./result/data_info', 'w') as f:
        f.write('num of data for each intention\n')
        for itr, intention in enumerate(pair_dict_key):
            f.write(intention +' : ' + str(data_info[itr])+'\n')
    print('-----------------------------\n')

    print('write total action data')
    #전체 action data
    with open('./result/intent_dataset.txt', 'w') as f:
        for intention in pair_dict_key:
            print(intention)
            for action in pair_dict[intention]:
                line = ','.join(action)
                line = line + '\t' + intention
                f.write(line+'\n')

                d_line = duplicate(action)

                if d_line:
                    for dupli in d_line:
                        dupli = ','.join(dupli)
                        dupli = dupli + '\t' + intention
                        f.write(dupli+'\n')
    print('-----------------------------\n')