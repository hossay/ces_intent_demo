from tf_model import TFModel
model = TFModel()

while True:
    action_lab, action_confidence, intent_lab, intent_confidence = model.run_demo_wrapper()
    print("\nClient:")
    print('action', action_lab, action_confidence)
    print('intent', intent_lab, intent_confidence)